package br.com.igormaldonado.springbootexample.open.rest;

import br.com.igormaldonado.springbootexample.model.Page;
import br.com.igormaldonado.springbootexample.mongo.PageDao;
import br.com.igormaldonado.springbootexample.mongo.PageRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/page")
public class PageService {

    @Autowired
    PageRepository pageRepository;

    @Autowired
    PageDao pageDao;

    @RequestMapping(value = "/count", method = RequestMethod.GET, produces="application/json; charset=UTF-8")
    public String countPages() {
        long start = System.currentTimeMillis();
        JSONObject json = new JSONObject();

//        List<Page> pages = pageRepository.findAll();
        json.put("quantity",pageRepository.count());

        long duration = System.currentTimeMillis() - start;
        json.put("durationMillis", duration);
        return json.toString(3);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces="application/json; charset=UTF-8")
    public String listPages() {
        long start = System.currentTimeMillis();
        JSONObject json = new JSONObject();

        List<Page> pages = pageRepository.findAll();
        json.put("quantity",pages.size());
        JSONArray pagesArray = new JSONArray();
        for(Page p : pages) {
            pagesArray.put(p.toJson());
        }
        json.put("pages", pagesArray);

        long duration = System.currentTimeMillis() - start;
        json.put("durationMillis", duration);
        return json.toString(3);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces="application/json; charset=UTF-8")
    public String registerSomePages(@RequestBody String jsonBodyAsString, @RequestParam(value = "bulk", required = false)  Boolean bulk, @RequestParam(value = "hide", required = false) Boolean hide) {
        long start = System.currentTimeMillis();
        JSONObject json = new JSONObject();
        JSONObject jsonBody = new JSONObject(jsonBodyAsString);
        if(bulk == null) {
            bulk = false;
        }
        if (hide == null) {
            hide = false;
        }

        if (jsonBody.has("quantity")) {
            int quantity = jsonBody.getInt("quantity");
            JSONArray array = new JSONArray();
            List<Page> bulkPending = new ArrayList<>();
            for (int i = 0; i < quantity; i++) {
                System.out.println("creating page: "+i);
                Page p = Page.getRandomPage();

                if(!bulk) {
                    pageRepository.save(p);
                } else {
                    bulkPending.add(p);
                }

                if (!hide) {
                    array.put(p.toJson());
                }
            }

            if(bulkPending.size() > 0 ) {
                System.out.println("Bulk started");
                pageDao.bulkInsert(bulkPending);
                System.out.println("Bulk ended");
            }

            json.put("status", "OK");
            json.put("createdPages", array);
        }

        long duration = System.currentTimeMillis() - start;
        json.put("durationMillis", duration);
        return json.toString(3);
    }

    @RequestMapping(value = "/findNear", method = RequestMethod.GET, produces="application/json; charset=UTF-8")
    public String findNearPages(@RequestParam("lat") Double lat, @RequestParam("lng") Double lng, @RequestParam(value = "distance", required = false, defaultValue = "15") Integer distance) {
        long start = System.currentTimeMillis();
        JSONObject json = new JSONObject();

        Point point = new Point(lat, lng);
        System.out.println("Distance: "+distance);
        Distance distanceObj = new Distance(distance, Metrics.KILOMETERS);

        List<Page> pages = pageDao.findClose(point, distanceObj);


        JSONArray arrayOfPages = new JSONArray();
        for (Page page: pages) {
            page.calculateDistanceFromLocation(point);
            arrayOfPages.put(page.toJson());
        }
        json.put("quantity", pages.size());
        json.put("pages",arrayOfPages);

        long duration = System.currentTimeMillis() - start;
        json.put("durationMillis", duration);
        return json.toString(3);
    }


}
