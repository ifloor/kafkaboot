package br.com.igormaldonado.springbootexample.open.rest;


import br.com.igormaldonado.springbootexample.model.Paycheck;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import example.avro.User;

@RestController
@RequestMapping("/paycheck")
public class PaycheckService {
    private static List<Paycheck> paychecks = new ArrayList<>();
    private static String BOOT_TOPIC = "topic";

    @Autowired
    private KafkaTemplate<String, example.avro.User> kafkaTemplate;

    public void sendMessage(User user) {
        kafkaTemplate.send(BOOT_TOPIC, user);
    }

    @RequestMapping(value="/list", method= RequestMethod.GET, produces="application/json; charset=UTF-8")
    public JSONArray list() {
        JSONArray paychecksArray = new JSONArray();
        for (Paycheck paycheck : paychecks ) {
            paychecksArray.put(paycheck);
        }

        return paychecksArray;
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String create(@RequestBody Paycheck paycheck) {
        paychecks.add(paycheck);
        JSONObject obj = new JSONObject();
        obj.put("message", "success");

        System.out.println("paycheck: "+paycheck.toString());
        User user = new User();
        user.setName("Usuarated");
        user.setFavoriteColor("Purple");
        user.setFavoriteNumber(12);

        sendMessage(user);

        return obj.toString(3);
    }

    @KafkaListener(topics = "topic", groupId = "dumb-consumer")
    public void listen(User user) {
        System.out.println("Received Message in group dumb-consumer: " + user.toString());
    }

}
