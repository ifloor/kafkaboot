package br.com.igormaldonado.springbootexample.mongo;

import br.com.igormaldonado.springbootexample.model.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface PageRepository extends MongoRepository<Page, String> {

    List<Page> findAllBy();

    List<Page> findByLocationNear(Point center, Distance distance);

}
