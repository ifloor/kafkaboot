package br.com.igormaldonado.springbootexample.mongo;

import br.com.igormaldonado.springbootexample.model.Page;
import com.mongodb.bulk.BulkWriteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PageDao {

    @Autowired
    private PageRepository pageRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public void bulkInsert(List<Page> pages) {
        BulkOperations bulkOperations = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, Page.class);
        BulkWriteResult result = bulkOperations.insert(pages).execute();
        System.out.println("result from bulk: "+result.toString());
    }

    public List<Page> findClose(Point center, Distance distance) {
        System.out.println("center: "+center.getX()+" - "+center.getY());
        System.out.println("Distance value: "+ distance.getValue());
        System.out.println("unit: "+distance.getUnit());
        Circle circle = new Circle(center, distance);
        Query query = Query.query(Criteria.where("location").withinSphere(circle));

//
//
//        return mongoTemplate.find(query, Page.class);

        return mongoTemplate.find(query, Page.class);
//        return pageRepository.findByLocationNear(center, distance);
    }
}
