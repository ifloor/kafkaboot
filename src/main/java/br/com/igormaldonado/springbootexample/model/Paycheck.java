package br.com.igormaldonado.springbootexample.model;

public class Paycheck {
    public String description;
    public Float value;
    public Float interestRate;

    public Paycheck() {

    }

    @Override
    public String toString() {
        return "Paycheck{" +
                "description='" + description + '\'' +
                ", value=" + value +
                ", interestRate=" + interestRate +
                '}';
    }
}
