package br.com.igormaldonado.springbootexample.model;

import com.google.common.base.CaseFormat;
import org.json.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

public class Page {

    @Id
    private String id;

    private String description;
    private String name;
    private GeoJsonPoint location;

    @Transient
    private Double distance;

    public Page() {

    }


    private static String[] randomWords = new String[]{"trail", "cover", "guard", "believe", "rigid", "hate", "bawdy", "color", "racial", "relax", "excellent", "shoes", "ablaze", "dare", "tacit", "consist", "puncture", "payment", "machine", "ducks", "shy", "jealous", "end", "mend", "wet", "distinct", "healthy", "stretch", "group", "talented", "drum", "drunk", "blushing", "moldy", "plan", "thundering", "nonstop", "fierce", "stormy", "wound", "recess", "ceaseless", "aggressive", "coil", "dress", "test", "uppity", "lopsided", "lavish", "unpack", "elderly", "stranger", "rare", "long-term", "assorted", "coordinated", "oranges", "explain", "kiss", "lovely", "aberrant", "form", "pushy", "smart", "driving", "distribution", "chubby", "husky", "worthless", "likeable", "food", "writing", "plantation", "feeble", "tease", "bat", "futuristic", "disappear", "inconclusive", "great", "airport", "motion", "knot", "cautious", "sack", "second-hand", "shut", "spill", "paste", "hug", "enchanted", "military", "heavenly", "visit", "decorous", "obsequious", "quicksand", "beds", "mix", "unite"};
    public static Page getRandomPage() {
        Page page = new Page();
        page.setName(Page.getRandomName());
        page.setDescription(Page.getRandomDescription());
        page.setLocation(Page.getRandomLocation());

        return page;
    }

    public void calculateDistanceFromLocation(Point point) {
        this.distance = Page.distance(this.location.getX(), point.getX(), this.location.getY(), point.getY(), 0, 0);
    }

    /**
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     *
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     * @returns Distance in Meters
     */
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }


    static private String getRandomName() {
        return randomWords[new Random().nextInt(randomWords.length)];
    }

    static private String getRandomDescription() {
        String randomDescription = "";
        Random rand = new Random();
        for(int i = 0; i < 10; i++) {
            String randomWord = randomWords[rand.nextInt(randomWords.length)];
            if(i == 0) {
                randomWord = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, randomWord);
            }

            randomDescription+=randomWord+" ";
        }

        return randomDescription;
    }

    static private GeoJsonPoint getRandomLocation() {
        double longitude = ThreadLocalRandom.current().nextDouble(-180.0, 180.0);
        double latitude = ThreadLocalRandom.current().nextDouble(-90.0, 90.0);

        return new GeoJsonPoint(longitude, latitude);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPoint location) {
        this.location = location;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public JSONObject toJson() {
        //Idiot "'s
        JSONObject object = new JSONObject();
        object.put("id", id);
        object.put("description", description);
        object.put("name", name);
        JSONObject jsonLocation = new JSONObject();
        jsonLocation.put("lat", location.getX());
        jsonLocation.put("lng", location.getY());
        object.put("location", jsonLocation);
        object.put("distance", distance);
        return object;
    }
}
